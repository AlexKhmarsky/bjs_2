// Теоретичні питання:
// 1. Які існують типи даних у Javascript?
// Number
// String
// Boolean
// Undefined
// Symbol
// BigInt
// Null
// Object

// 2. У чому різниця між == і ===?
// Оператор == порівнює на рівність, оператор === на ідентичність та він не наводить значення до одного типу.

// 3. Що таке оператор?
// Оператор це елемент мови, задаючий повний опис дій, які необхідно виконати.

//Завдання
// Реалізувати просту програму на Javascript, яка взаємодіятиме з користувачем за допомогою модальних вікон
// браузера - alert, prompt, confirm. Завдання має бути виконане на чистому Javascript без використання бібліотек
// типу jQuery або React.
//
// Технічні вимоги:
// Отримати за допомогою модального вікна браузера дані користувача: ім'я та вік.
// Якщо вік менше 18 років - показати на екрані повідомлення: You are not allowed to visit this website.
// Якщо вік від 18 до 22 років (включно) – показати вікно з наступним повідомленням: Are you sure you want to continue?
// і кнопками Ok, Cancel. Якщо користувач натиснув Ok, показати на екрані повідомлення: Welcome, + ім'я користувача.
// Якщо користувач натиснув Cancel, показати на екрані повідомлення: You are not allowed to visit this website.
// Якщо вік більше 22 років – показати на екрані повідомлення: Welcome, + ім'я користувача.
// Обов'язково необхідно використовувати синтаксис ES6 (ES2015) для створення змінних.
// Після введення даних додати перевірку їхньої коректності. Якщо користувач не ввів ім'я, або при введенні віку
// вказав не число - запитати ім'я та вік наново (при цьому дефолтним значенням для кожної зі змінних має бути введена
// раніше інформація).

let userName = prompt ("What is your name ?");
let userAge = prompt ("How old are you ?");

while (!userName || !isNaN(userName) || userName == null || /\d/.test(userName)) {
    userName = prompt("Please, enter your name correctly !");
}
while (!userAge || userAge === "" || userAge == null || isNaN(userAge)) {
    userAge = prompt("Please, enter your age correctly !");
}
if (userAge < 18) {
    alert("You are not allowed to visit this website.");
}
if (userAge > 22) {
    alert("Welcome, " + (userName));
}
if (userAge >= 18 && userAge <= 22) {
    let ageAccept = confirm("Are you sure you want to continue ?");
    if (ageAccept === true) {
        alert("Welcome, " + (userName));
    } if (ageAccept === false) {
        alert("You are not allowed to visit this website.")
    }
}


